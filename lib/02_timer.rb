class Timer
  def initialize(start_time=0)
    @time=start_time
  end

  def seconds
    @time
  end

  def seconds= (new_time)
    @time = new_time
  end

  def time_string
    string=[]
    string.push(@time%60)
    string.push(@time/60 % 60)
    string.push(@time/60/60 %24)
    string.reverse.map do |just|
      just.to_s.rjust(2,'0')
    end.join(":")
  end
end
