class Book
  attr_accessor :title
  def initialize
    @book
  end

  def title
    new_title=[]
    conjunctions=['and','the','a','in','of','an']
    @title.split(" ").each_with_index do |word,idx|
      if conjunctions.include?(word) && idx != 0
        new_title.push(word)
        next
      end
      new_title.push(word[0].upcase+word[1..-1])
    end
    @title=new_title.join(" ")
  end

end
