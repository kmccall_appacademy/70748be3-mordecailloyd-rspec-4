class Temperature
  def initialize(hash)
    @temperature_hash = hash
  end
  def in_fahrenheit
    if @temperature_hash[:c]
      return ((@temperature_hash[:c] * (9.0/5.0)) + 32)
    end
    @temperature_hash[:f]
  end

  def in_celsius
    if @temperature_hash[:f]
      return (@temperature_hash[:f] - 32) * (5.0/9.0)
    end
    @temperature_hash[:c]
  end

  def self.from_celsius(value)
    self.new({:c=>value})
  end

  def self.from_fahrenheit(value)
    self.new({:f=>value})
  end

end

class Celsius < Temperature
  def initialize (value)
    @temperature_hash = {:c => value }
  end


end

class Fahrenheit < Temperature
  def initialize (value)
    @temperature_hash = {:f => value}
  end

end
